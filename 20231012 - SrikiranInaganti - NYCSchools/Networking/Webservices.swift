//
//  Webservices.swift
//  20231012 - SrikiranInaganti - NYCSchools
//
//  Created by SRIKIRAN INAGANTI on 10/12/23.
//

import Foundation

protocol NetworkService {
    func fetchSchools(completion: @escaping (Result<[School], Error>) -> Void)
}

class WebService: NetworkService {
    enum Constants {
        static let FETCH_TIMEOUT = 15.0
        static let URL_STRING = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json"
        static let FATAL_ERROR_URL = "FATAL ERROR: URL could not be created"
    }
    
    private let urlString: String
    
    init(urlString: String = Constants.URL_STRING) {
        self.urlString = urlString
    }

    private func parseJSON(fromData data: Data) -> Result<[School], Error> {
        do {
            let decodedData = try JSONDecoder().decode([School].self, from: data)
            return .success(decodedData)
        } catch {
            return .failure(error)
        }
    }
    
    private func processJSONRequest(data: Data?, error: Error?) -> Result<[School], Error> {
        guard let jsonData = data else {
            return .failure(error!)
        }
        
        return parseJSON(fromData: jsonData)
    }
    
    func fetchSchools(completion: @escaping (Result<[School], Error>) -> Void) {
        if let url = URL(string: self.urlString) {
            let request = URLRequest(url: url)
            let sessionConfig = URLSessionConfiguration.default
            sessionConfig.timeoutIntervalForRequest = Constants.FETCH_TIMEOUT
            let session = URLSession(configuration: sessionConfig)
            session.dataTask(with: request) {
                (data, response, error) in
                
                let result = self.processJSONRequest(data: data, error: error)
                DispatchQueue.main.async {
                    completion(result)
                }
            }.resume()
        } else {
            preconditionFailure(Constants.FATAL_ERROR_URL)
        }
    }
}
