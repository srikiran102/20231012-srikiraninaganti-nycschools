//
//  SchoolVM.swift
//  20231012 - SrikiranInaganti - NYCSchools
//
//  Created by SRIKIRAN INAGANTI on 10/12/23.
//

import Foundation


class SchoolVM {
    
    var schools = [School]()
    var fetchedDataSuccessfully = false
    
    let webService: NetworkService
    
    init(webService: NetworkService) {
        self.webService = webService
    }
    
    func fetchSchoolsFromWeb() {
        webService.fetchSchools() { (result) in
            switch result {
            case let .success(schools):
                self.schools = schools.sorted { $0.schoolName < $1.schoolName }
                self.fetchedDataSuccessfully = true
                
            default:
                // the failure case
                break
            }
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: ListViewController.Constants.NOTIFY_RELOAD),
                                            object: nil)
        }
    }
}
