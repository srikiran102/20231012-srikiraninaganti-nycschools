//
//  MockWebService.swift
//  20231012 - SrikiranInaganti - NYCSchoolsTests
//
//  Created by SRIKIRAN INAGANTI on 10/12/23.
//

import Foundation

class MockWebService: NetworkService {
    
    enum MockError: Error {
        case error
    }
    
    private let urlString: String
    
    init(urlString: String = "Mock URL") {
        self.urlString = urlString
    }
    
    
    private var mockSchools = [
        School(dbNumber: "0000",
               schoolName: "Mock School Name 1",
               numberOfTakers: "1",
               averageReadingScore: "800",
               averageWritingScore: "700",
               averageMathScore: "600"),
        School(dbNumber: "0001",
               schoolName: "Mock School Name 2",
               numberOfTakers: "2",
               averageReadingScore: "500",
               averageWritingScore: "400",
               averageMathScore: "300")
    ]
    
    func fetchSchools(completion: @escaping (Result<[School], Error>) -> Void) {
        if !urlString.isEmpty {
            completion(.success(mockSchools))
        } else {
            completion(.failure(MockError.error))
        }
    }
}
