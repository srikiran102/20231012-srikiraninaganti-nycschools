//
//  _0231012___SrikiranInaganti___NYCSchoolsTests.swift
//  20231012 - SrikiranInaganti - NYCSchoolsTests
//
//  Created by SRIKIRAN INAGANTI on 10/12/23.
//

import XCTest
@testable import _0231012___SrikiranInaganti___NYCSchools

class schoolViewModelTest: XCTestCase {
    var schoolStoreVM: SchoolVM!
    
    override func setUp() {
        // making an instance with the default parameters will lead to a mock success fetch
        schoolStoreVM = SchoolVM(webService: MockWebService())
    }

    func test_create_empty_school_view_model() {
        XCTAssertEqual(schoolStoreVM.schools.count, 0)
        XCTAssertEqual(schoolStoreVM.fetchedDataSuccessfully, false)
    }
}

class mock_web_service: XCTestCase {
    func test_fetching_success() {
        // making an instance with the default parameters will lead to a mock success fetch
        let schoolStoreVM = SchoolVM(webService: MockWebService())
        schoolStoreVM.fetchSchoolsFromWeb()
        XCTAssertEqual(schoolStoreVM.schools.count, 2)
        XCTAssertEqual(schoolStoreVM.fetchedDataSuccessfully, true)
    }
    
    func test_fetching_failure() {
        // putting in an empty string is the way to force a mock failure
        let schoolStoreVM = SchoolVM(webService: MockWebService(urlString: ""))
        schoolStoreVM.fetchSchoolsFromWeb()
        XCTAssertEqual(schoolStoreVM.schools.count, 0)
        XCTAssertEqual(schoolStoreVM.fetchedDataSuccessfully, false)
    }
}
